//
//  TimeConversionApp.swift
//  TimeConversion
//
//  Created by Jose Antonio on 14/8/22.
//

import SwiftUI

@main
struct TimeConversionApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
