import SwiftUI

enum Units: String, CaseIterable {
    case seconds
    case minutes
    case hours
    case days
}

struct ContentView: View {
    
    @FocusState private var textFileIsFocused: Bool
    @State private var inputUnit: Units = .seconds
    @State private var outputUnit: Units = .hours
    @State private var enteredValue = ""
//    @State private var resultOfConversion = 0
    
    private var inputValueInSeconds: Int {
        guard enteredValue != "" else { return 0 }
        
        switch inputUnit {
        case .seconds:
            return Int(enteredValue)!
        case .minutes:
            return Int(enteredValue)! * 60
        case .hours:
            return Int(enteredValue)! * 60 * 60
        case .days:
            return Int(enteredValue)! * 60 * 60 * 24
        }
    }
    
    private var resultOfConversion: Int {
        guard enteredValue != "" else { return 0 }
        
        switch outputUnit {
        case .seconds:
            return inputValueInSeconds
        case .minutes:
            return inputValueInSeconds / 60
        case .hours:
            return inputValueInSeconds / (60 * 60)
        case .days:
            return inputValueInSeconds / (60 * 60 * 24)
        }
    }
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    Picker("Convert from", selection: $inputUnit) {
                        ForEach(Units.allCases, id: \.self) {
                            Text($0.rawValue.capitalized)
                        }
                    }
                    .pickerStyle(.segmented)
                } header: {
                    Text("Convert from")
                }
                
                Section {
                    Picker("Convert to", selection: $outputUnit) {
                        ForEach(Units.allCases, id: \.self) {
                            Text($0.rawValue.capitalized)
                        }
                    }
                    .pickerStyle(.segmented)
                } header: {
                    Text("Convert to")
                }
                
                Section {
                    TextField("Enter number of \(inputUnit.rawValue)", text: $enteredValue)
                        .keyboardType(.numberPad)
                        .focused($textFileIsFocused)
                    Text("\(enteredValue) \(inputUnit.rawValue) is equal to \(resultOfConversion) \(outputUnit.rawValue)")
                }
            }
            .navigationTitle("Time Conversion")
            .toolbar {
                ToolbarItemGroup(placement: .keyboard) {
                    Spacer()
                    Button("Done") {
                        textFileIsFocused = false
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
